/*
 * cuda-me.h
 *
 *  Created on: Jan 12, 2009
 *      Author: rothberg
 */

#ifndef CUDAME_H_
#define CUDAME_H_

#ifdef __CUDACC__
  #include "hier.h"
#endif


extern "C" void cuda_me(x264_t *h, int** mvX, int** mvY);
extern "C" void cuda_me2(x264_t *h, int** mvX, int** mvY);


#endif /* CUDAME_H_ */
